import React from 'react';
import HookSwitcher from '../HookSwitcher';

import './App.css';

const App = () => {


    return(
        <React.Fragment>
            <HookSwitcher/>
        </React.Fragment>
    );
};

export default App;