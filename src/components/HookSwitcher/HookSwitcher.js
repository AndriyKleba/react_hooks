import React from 'react';

import './HookSwitcher.css'

const HookSwitcher = () => {

    const [color, setColor] = React.useState('white');
    const [fontSize, setFontSize] = React.useState(14)

    return (
        <div style={{padding: '10px', backgroundColor: color, fontSize: `${fontSize}px`}}>
            <h1>Hi world</h1>
            <button
                className="white-btn"
                onClick={() => setColor('white')}
            >Світло
            </button>
            <button
                className="dark-btn"
                onClick={() => setColor('black')}
            >Тьма
            </button>
            <button onClick={() => setFontSize((state) => state + 2)}>
                +
            </button>
            <button onClick={() => setFontSize((state) => state - 2)}>
                -
            </button>
        </div>
    )
}

export default HookSwitcher;